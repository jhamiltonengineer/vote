FROM ubuntu:16.04
MAINTAINER "Rene Gielen" <rgielen@apache.org>


RUN apt-get update
RUN apt-get install -y wget lsb-release software-properties-common

RUN add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main"

RUN apt-get update --allow-unauthenticated \
      && apt-get install --allow-unauthenticated -y \
           postgresql-9.6 \	
      && apt-get clean \
      && rm -rf /var/lib/apt/lists/* \
      && rm -rf /tmp/*

COPY send.sh /usr/bin

CMD ["/usr/bin/send.sh"]

